require('dotenv').config({ path: `${__dirname}/.env` });
const TelegramBot = require('node-telegram-bot-api');
const axios = require('axios');
const parser = require('fast-xml-parser');
const sortBy = require('lodash.sortby');
const fs = require('fs');

const token = process.env.BOT_TOKEN;

const bot = new TelegramBot(token, {polling: true});

const projectsToString = (projects) => {
  return projects.reduce((acc, current) => {
    return `${acc}<b>${current.title}</b>
${current.description.slice(0, 300).replace('\n',' ')}
<a href="${current.link}">Подробнее</a>\n\n\n`;
  }, '');
}

bot.on('message', (msg) => {
  const tasks = JSON.parse(fs.readFileSync(`${__dirname}/data.json`));
  const {newTasks, keyboard} = tasks.items.reduce((acc, task) => {
    const newAcc = acc;

    const newTask = task;
    if (msg.text.replace(' ✅', '') === task.category) {
      const index = task.listeners.indexOf(msg.from.id);
      if (index > -1) {
        newTask.listeners.splice(index, 1);
      } else {
        newTask.listeners.push(msg.from.id);
      }
    }
    newAcc.newTasks.push(newTask);

    const name = newTask.listeners.indexOf(msg.from.id) > -1 ? `${newTask.category} ✅` : newTask.category;
    newAcc.keyboard.push([{text: name}]);

    return newAcc;
  }, {
    newTasks: [],
    keyboard: []
  });
  tasks.items = newTasks;
  fs.writeFileSync(`${__dirname}/data.json`, JSON.stringify(tasks));

  bot.sendMessage(msg.from.id, 'Выберите категории, которые хотите отслеживать или отключить их отслеживание\n' +
    'Отслеживаемые категории помечены галочкой. Чтобы перестать отслеживать ее, повторно нажмите на нее.', {
    reply_markup: {
      keyboard
    },
    parse_mode: 'html'
  });
})

const sendNewProjects = async (task, newProjects) => {
  const appropriateProjects = newProjects.filter((project) => {
    if (Array.isArray(project.category)){
      project.category.forEach((category) => {
        if(category.split(' / ')[0] === task.category) {
          return true;
        }
      });
    } else {
      return project.category.split(' / ')[0] === task.category;
    }
    return false;
  });
  if (appropriateProjects.length > 0) {
    const projectString = projectsToString(appropriateProjects);
    task.listeners.forEach((listener) => {
      bot.sendMessage(listener, projectString, {
        parse_mode: 'html',
        disable_web_page_preview: true
      });
    });
  }
}

setInterval(async () => {
  const tasks = JSON.parse(fs.readFileSync(`${__dirname}/data.json`));
  const url = `https://www.fl.ru/rss/`;
  const response = await axios.get(url);
  if (parser.validate(response.data) === true) {
    const projects = (parser.parse(response.data)).rss.channel.item;
    const projectsWithDates = projects.map((project) => {
      const newProject = project;
      project.date = new Date(project.pubDate);
      return newProject;
    });
    const newProjects = projectsWithDates.filter((project) => {
      return project.date > new Date(tasks.lastDate);
    });
    if (newProjects.length > 0) {
      const someNewProjects = sortBy(newProjects, 'date').slice(0, 10);
      const newTasks = tasks;
      newTasks.lastDate = someNewProjects[someNewProjects.length-1].date;
      tasks.items.forEach((task) => {
        sendNewProjects(task, someNewProjects);
      });
      fs.writeFileSync(`${__dirname}/data.json`, JSON.stringify(newTasks));
    }
  }
}, 5000)
