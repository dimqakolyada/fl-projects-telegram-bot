FROM node:lts-alpine

WORKDIR /app

RUN npm install -g nodemon

COPY package.json .

RUN npm install --only=prod

COPY . .

VOLUME [ "./data.json" ]

ENTRYPOINT npm run start