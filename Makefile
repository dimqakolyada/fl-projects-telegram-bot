run:
	docker run --restart=unless-stopped -d --name fl-projects-telegram-bot --env-file ./.env -v "$(shell pwd)/data.json:/app/data.json" registry.gitlab.com/dimqakolyada/fl-projects-telegram-bot

build:
	docker build -t registry.gitlab.com/dimqakolyada/fl-projects-telegram-bot .

stop:
	docker stop fl-projects-telegram-bot
	docker rm fl-projects-telegram-bot

publish:
	docker login registry.gitlab.com
	docker push registry.gitlab.com/dimqakolyada/fl-projects-telegram-bot

exec:
	docker exec -it fl-projects-telegram-bot sh

logs:
	docker logs --tail 1000 -f fl-projects-telegram-bot